#include "log.h"
Logger::Logger(int buffSize, int filesCurrent) {
	Logger::filesMax = filesCurrent;
	Logger::buffSize = buffSize;
}
Logger::~Logger() {
	if (!buffer.empty()) save_log_file();
}
void Logger::save_log_file() {
	is_max_file();
	currentTime = std::time(nullptr);
	filename = std::to_string(currentTime) + "(" + std::to_string(++filesCurrent) + ")"; filesCount++;
	logFile.open(filename, logFile.out);
	logFile.write(buffer.c_str(), buffer.size());
	std::cout << "Create " << filename << std::endl;
	logFile.close();
	fileNames.push(filename);
}
void Logger::is_max_file() {
	if (filesCount >= filesMax) {
		std::string onRemove = fileNames.front(); fileNames.pop();
		unlink(onRemove.c_str());
		std::cout << onRemove << " has been removed." << std::endl;

		filesCount--;
	}
}
void Logger::is_max_buffer() {
	save_log_file();
	buffer.clear();
	std::cout << "Old messages been saved as " << filename << std::endl;
	log_in("Old messages been saved as " + filename + "\n\n");
}
void Logger::log_in(std::string message) {
	if (message.size() > buffSize) {
		std::cout << "Error! Message more then buffer size!" << std::endl;
		return;
	}
	if (buffer.size() + message.size() > buffSize) is_max_buffer();
	buffer += message;
}
void Logger::log_out() {
	std::cout << buffer << std::endl;
}
