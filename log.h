#include <iostream>
#include <fstream>
#include <ctime>
#include <queue>
#include <cstring>
#include <unistd.h>
class Logger {
private:
	std::string buffer; int buffSize;
	int filesCurrent = 0, filesCount = 0, filesMax; std::queue<std::string> fileNames; std::string filename;
	std::time_t currentTime; std::string ccurrentTime;
	std::fstream logFile;
public:
	Logger(int buffSize, int filesCurrent);
	~Logger();

	void save_log_file(); 
	
	void is_max_file(); 
	void is_max_buffer(); 

	void log_in(std::string message);
	void log_out();
};